
# Contao Notification Center Monitor Bundle

## About
This bundle can be used to monitor the queues within the notification-center.
It can be setup with a cronjob, to look for errors or unsent e-mails in the queues.
The information to errors/unset e-mails are sent to the "kundenliste" via API.
The PRTG (monitoring-system) can collect all data to the different websites via the same api, with this call:
http://kundenliste.memoserver.ch/queue/readall

## Installation (German)
### 1. Git-Repo im composer.json hinterlegen hinterlegen
Da dieses Bundle nicht gelistet wird (auf Packagist), muss man das Repo manuell im composer.json hinterlegen.
```
"repositories": [
  {
    "type": "vcs",
    "url" : "https://bitbucket.org/memo_development/contao-nc-monitor-bundle.git"
  }
],
```

### 2. Bundle im Composer hinzufügen
Im Composer.json im "require" Segment einen neuen Eintrag hinzufügen:
```
"memo_development/contao-nc-monitor-bundle": "^1.0"
```

### 3. Composer Update durchführen
z.B. via xy.ch/contao-manager.phar.php oder lokal mit:
```
composer update
```

### 4. Datenbankupdate durchführen
Über mydomain.ch/contao/install oder per Console:
```
php bin/console contao:migrate
```
das Datenbankupdate durchführen.

### 5. Zu überwachende Warteschlangen und die Domäne definieren
Im Backend müsste nun ein neuer Navigations-Punkt existieren "QUEUE MONITOR".
Darin muss man die Domäne (Live-Domäne ohne www) eingeben/korrigieren und die zu überwachende Queue auswählen.
**WICHTIG:** Zur Sicherheit unserer Infrastruktur wird kontrolliert, ob die Domäne in der Kundenliste hinterlegt ist, sprich diese muss entsprechend im Bexio geführt werden und dann per Sync in die Kundenliste importiert werden! Bei Unklarheiten, Rory Fragen ;)

### 6. Cronjob einrichten
Im Hosting einen neuen Cronjob für all 5 Minuten einrichten.
**WICHTIG:** Wert für die "Minuten" sollte somit ```*/5``` sein und alle anderen```*```

Der Befehl müsste ca. so aussehen (Ordner anpassen):
```
/usr/local/bin/php73 /home/kundenname/public_html/contao49/vendor/bin/contao-console memo:checkncqueue
```

### 7. Im PRTG checken
Neue Nachricht erstellen und mindestens 5 min in der Warteschlange belassen.
Der Sensor sollte ausschlagen, dann kann die Nachricht verschickt werden und der Sensor sollte wieder Grün werden.
Ansonsten mit Rory anschauen ;)
