<?php
/**
 * @package   NCMonitorBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\NCMonitorBundle\Command;

use Contao\CoreBundle\Framework\ContaoFramework;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

use \Memo\NCMonitorBundle\Service\QueueMonitor;

class CheckQueueCommand extends Command
{
	protected static $defaultName = 'memo:checkncqueue';
	protected $framework;
	private $io;
	private $rows = [];
	private $statusCode = 0;

	public function __construct(ContaoFramework $contaoFramework)
	{
		$this->framework = $contaoFramework;
		$this->framework->initialize();
		parent::__construct();
	}

	protected function configure(): void
	{
		$this->setDescription('Fehler und Stau in Notification Queue erkennen');
	}

	protected function execute(InputInterface $input, OutputInterface $output): ?int
	{
		$this->statusCode = 0;
		$objQueueMonitor = new QueueMonitor();

		if($objQueueMonitor->checkQueues()){

			$this->statusCode = 1;

			$this->io = new SymfonyStyle($input, $output);
			$this->io->text('Error while checking the notification-center queues - please check all settings');

		}

		return $this->statusCode;
	}
}
