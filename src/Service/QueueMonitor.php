<?php
/**
 * @package   NCMonitorBundle
 * @author	Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */
 
namespace Memo\NCMonitorBundle\Service;

use Contao\Controller;
use NotificationCenter\Model\QueuedMessage;
use Memo\NCMonitorBundle\Model\NotificationMonitorModel;

class QueueMonitor {

	static function callURL($method, $url, $data = false)
	{
		$curl = curl_init();
		
		switch ($method)
		{
			case "POST":
				curl_setopt($curl, CURLOPT_POST, 1);
		
				if ($data)
					curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
				break;
			case "PUT":
				curl_setopt($curl, CURLOPT_PUT, 1);
				break;
			default:
				if ($data)
					$url = sprintf("%s?%s", $url, http_build_query($data));
		}
		
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		
		$result = curl_exec($curl);
		
		curl_close($curl);
		
		return $result;
	}

	static function checkQueue($objQueue) {
			
		//LastCheck Datum definieren
		$strThisCheck = time();
		
		//Letzter Check auslesen
		$objDatabase = \Database::getInstance();
		$objLastCheck = $objDatabase->prepare("SELECT * FROM tl_mod_queue_monitor WHERE tstamp > 0 AND queue_id = '".$objQueue->queue_id."' ORDER BY tstamp DESC LIMIT 1")->execute();
		
		$intLastQueued = $objQueue->queued;
		$intLastErrors = $objQueue->errors;
	
		//Fehler finden
		$collErrors = QueuedMessage::findBy(array('error=?', 'sourceQueue=?'), array(1, $objQueue->queue_id));
		
		if($collErrors){
			$intCountErrors = count($collErrors);
		} else {
			$intCountErrors = 0;
		}
		
		//Queued Messages suchen
		$intWaitToSend = 300;
		$intDateToLong = time() - $intWaitToSend;
		
		$collQueued = QueuedMessage::findBy(array('error!=?', 'dateSent=?', 'dateAdded<?', 'sourceQueue=?'), array(1, '', $intDateToLong, $objQueue->queue_id));
		
		if($collQueued){
			$intCountQueued = count($collQueued);
		} else {
			$intCountQueued = 0;
		}
		
		if($intLastQueued != $intCountQueued || $intCountErrors != $intLastErrors){
		
			$strURL = "https://kundenliste.memoserver.ch/queue/update/" . $objQueue->domain . "/id/" . $objQueue->queue_id . "/errors/" . $intCountErrors . "/queued/" . $intCountQueued;
			
			$strAPIResult = self::callURL('GET', $strURL);
			
			
			//Eintrag updaten
			$objQueue->last_cron = $strThisCheck;
			$objQueue->errors = $intCountErrors;
			$objQueue->queued = $intCountQueued;
			$objQueue->last_api_result = $strAPIResult;
			$objQueue->save();
	
			//Log nachführen und Mediamotion informieren
			if($intCountQueued > 0){
				\System::log('Alte Mails in Mail-Queue gefunden, ' . $intCountQueued . ' Mails in der Warteschlange unversendet', 'QueueMonitor', TL_ERROR);
			}
			if($intCountErrors > 0){
				\System::log('Error in Mail-Queue gefunden, ' . $intCountErrors . ' neue Mails in der mit Fehler gefunden', 'QueueMonitor', TL_ERROR);
			}
			
		} elseif($objQueue->last_cron == 0){
			
			$strURL = "https://kundenliste.memoserver.ch/queue/update/" . $objQueue->domain . "/id/" . $objQueue->queue_id . "/errors/" . $intCountErrors . "/queued/" . $intCountQueued;
			
			$strAPIResult = self::callURL('GET', $strURL);
			
			//Eintrag updaten
			$objQueue->last_cron = $strThisCheck;
			$objQueue->errors = $intCountErrors;
			$objQueue->queued = $intCountQueued;
			$objQueue->last_api_result = $strAPIResult;
			$objQueue->save();
			
		}
	}

	public function checkQueues() {
		

		$objQueues = NotificationMonitorModel::findAll();
		
		if($objQueues){
			while($objQueues->next()){
				self::checkQueue($objQueues);
			}
		} else {
			return "Keine Queue zur Überwachung im Backend erfasst!";
		}
	}
}