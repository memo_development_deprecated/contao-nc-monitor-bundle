<?php
/**
 * @package   NCMonitorBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\NCMonitorBundle\Model;

/**
 * Class NotificationMonitorModel
 *
 * Reads and writes NotificationMonitorModel.
 */
class NotificationMonitorModel extends \Model
{
	/**
     * Table name
     * @var string
    **/
    protected static $strTable = 'tl_mod_queue_monitor';

}