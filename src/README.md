#Contao 4 Notification Monitor Bundle  

## Funktionsumfang  
Dieses Bundle sucht nach Fehlern und nicht versendeten (nach 5 min) Mails in den Notification Center Queue.  

## Installation  

### 1. Bundle hochladen  
Das Bundle in den /src/Memo/ Ordner im Root-Verzeichnis platzieren.  
  
### 1.1. ContaoManager anlegen oder erweitern
Falls der ContaoManager bereits im src/ContaoManager/ContaoManagerPlugin.php vorhanden ist, kann man das neue Bundle einfach im bestehenden anmelden.  
Alternativ kann man folgenden Manager als Beispiel nehmen (min Catalog, Cookie und Monitor Bundles):   
 
```
<?php

use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;

use Memo\ModCatalogBundle\ModCatalogBundle;
use Memo\ModCookieBarBundle\ModCookieBarBundle;
use Memo\ModNotificationMonitorBundle\ModNotificationMonitorBundle;

class ContaoManagerPlugin implements BundlePluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(ModCatalogBundle::class)
            	->setLoadAfter(
                    [
                        'Contao\CoreBundle\ContaoCoreBundle',
                        'Contao\CalendarBundle\ContaoCalendarBundle',
                        'Contao\FaqBundle\FaqBundle',
                        'MenAtWork\MultiColumnWizard',
                        'Sioweb\GlossarBundle\SiowebGlossarBundle'
                    ]
                ),
            BundleConfig::create(ModCookieBarBundle::class)->setLoadAfter([ContaoCoreBundle::class]),
            BundleConfig::create(ModNotificationMonitorBundle::class)->setLoadAfter([ContaoCoreBundle::class])
        ];
    }
}
```  
Wichtig dabei sind zwei Zeilen, das "use" Statement:  
```
use Memo\ModNotificationMonitorBundle\ModNotificationMonitorBundle;
```  
und das BundleConfig Anmeldung:  
```
BundleConfig::create(ModNotificationMonitorBundle::class)->setLoadAfter([ContaoCoreBundle::class]) 
```  
  
### 2. Bundle in Composer anmelden
```
#!bash shell scripts
 "autoload": {
        "classmap": [
            "src/ContaoManager/ContaoManagerPlugin.php"
        ],
        "psr-4": {
            "Memo\\ModNotificationMonitorBundle\\": "src/Memo/ModNotificationMonitorBundle/"
        }
    },
```
  
### 4. Neues Bundle einlesen  
Auf der Console folgenden Befehl ausführen:  

```
#!bash shell scripts
composer install --optimize-autoloader
```  
  
### 4. Datenbankupdate durchführen
Über mydomain.ch/contao/install das Datenbankupdate durchführen
  
### 5. Zu überwachende Warteschlangen und die Domäne definieren
Im Backend müsste nun ein neuer Navigations-Punkt existieren "QUEUE MONITOR".  
Darin muss man die Domäne (ohne www. oder memoserver.ch) eingeben/korrigieren und die Queue auswählen.  

### 6. Cronjob einrichten
Im Hosting einen neuen Cronjob für all 5 Minuten einrichten.  
WICHTIG: Wert für die "Minuten" sollte somit ```*/5``` sein und alle anderen```*```  

Der Befehl müsste ca. so aussehen (Ordner anpassen):  
```
#!bash shell scripts
/usr/local/bin/php71 /home/snvch/public_html/contao4-managed/src/Memo/ModNotificationMonitorBundle/Resources/contao/bin/check
```  

### 7. Im PRTG checken (evtl. via Rory)
Kontrollieren, ob der neue Kunde eingelesen wurde und der "Max-Wert" auf 0 gesetzt wurde.  
Ansonsten zeichnet der PRTG die Mails zwar auf, aber meldet keinen Error.  
