<?php
/**
 * @package   NCMonitorBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\NCMonitorBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MemoNCMonitorBundle extends Bundle
{
}
