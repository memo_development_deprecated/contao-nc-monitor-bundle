<?php
/**
 * @package   NCMonitorBundle
 * @author    Rory Z�nd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */
 
 /**
 * Add back end modules
 */

if (! array_key_exists('queue_monitor', $GLOBALS['BE_MOD'])) {
    array_insert($GLOBALS['BE_MOD'], 10, array('queue_monitor' => array()));
}

array_insert($GLOBALS['BE_MOD']['queue_monitor'], 0, array
(
	'queues' => array
	(
		'tables' 	=> array('tl_mod_queue_monitor')
	))
);
 
 /**
 * Models
 */

$GLOBALS['TL_MODELS']['tl_mod_queue_monitor']          = 'Memo\NCMonitorBundle\Model\NotificationMonitorModel';

/**
 * Backend CSS
 */
if(TL_MODE == 'BE') 
{ 
	$GLOBALS['TL_CSS'][]	= 'bundles/memoncmonitor/backend_extended.css?v=20200614'; 
}