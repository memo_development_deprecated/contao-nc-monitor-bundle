<?php
/**
 * @package   NCMonitorBundle
 * @author    Rory Z�nd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

use NotificationCenter\Model\Gateway;

/**
 * Table tl_mod_queue_monitor
 */
$GLOBALS['TL_DCA']['tl_mod_queue_monitor'] = array
(

	// Config
	'config' => array
	(
		'dataContainer'               => 'Table',
		'enableVersioning'            => true,
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary'
			)
		)
	),

	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 2,
			'fields'                  => array('domain'),
			'flag'                    => 12,
			'panelLayout'			  => 'search, filter; sort, limit',
			'disableGrouping'		  => true
		),
		'label' => array
		(
			'fields'                  => array('domain'),
		),
		'global_operations' => array
		(
			'all' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset();" accesskey="e"'
			)
		),
		'operations' => array
		(
			'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_mod_queue_monitor']['edit'],
				'href'                => 'act=edit',
				'icon'                => 'edit.gif'
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_mod_queue_monitor']['copy'],
				'href'                => 'act=copy',
				'icon'                => 'copy.gif'
			),

			'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_mod_queue_monitor']['delete'],
				'href'                => 'act=delete',
				'icon'                => 'delete.gif',
				'attributes'          => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"'
			),
			'show' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_mod_queue_monitor']['show'],
				'href'                => 'act=show',
				'icon'                => 'show.gif'
			),
		)
	),

	// Select
	'select' => array
	(
		'buttons_callback' => array()
	),

	// Edit
	'edit' => array
	(
		'buttons_callback' => array()
	),

	// Palettes
	'palettes' => array
	(
		'default'                     => '{general_legend},domain, queue_id, queued, errors, last_cron, last_api_result;',
	),

	// Subpalettes
	'subpalettes' => array
	(
		''                            => '',
	),

	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL auto_increment"
		),
		'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'last_cron' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_mod_queue_monitor']['last_cron'],
			'exclude'                 => true,
			'filter'                  => false,
			'search'                  => false,
			'sorting'                 => true,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>false, 'rgxp'=>'datim', 'tl_class'=>'long clr wizard', 'readonly' => true),
			'sql'                     => "int(10) unsigned NOT NULL default '0'",
		),
		'last_api_result' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_mod_queue_monitor']['last_api_result'],
			'exclude'                 => true,
			'filter'                  => false,
			'search'                  => false,
			'sorting'                 => false,
			'inputType'               => 'textarea',
			'eval'                    => array('mandatory'=>false, 'tl_class'=>'clr long', 'readonly' => true),
			'sql'                     => "mediumtext NULL"
		),
		'domain' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_mod_queue_monitor']['domain'],
			'inputType'               => 'text',
			'default'				  => empty($_SERVER['SERVER_NAME'])? '' : $_SERVER['SERVER_NAME'],
			'filter'				  => true,
			'sorting'				 => true,
			'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'tl_class' => 'w50'),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'queue_id' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_mod_queue_monitor']['queue_id'],
			'exclude'                 => true,
			'sorting'				  => true,
			'filter'				  => true,
			'inputType'               => 'select',
			'options_callback'        => array('tl_mod_queue_monitor', 'getQueues'),
			'foreignKey'              => 'tl_nc_gateway.title',
			'eval'                    => array('mandatory'=>true, 'tl_class'=>'w50', 'chosen'=>true, 'multiple'=>false, 'includeBlankOption'=>true),
			'sql'                     => "int(4) NULL",
			'relation'                => array('type'=>'hasOne', 'load'=>'eager', 'field'=>'id')
		),
		'queued' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_mod_queue_monitor']['queued'],
			'inputType'               => 'text',
			'filter'				  => true,
			'sorting'				 => true,
			'eval'                    => array('maxlength'=>255, 'tl_class' => 'w50', 'readonly' => true),
			'sql'                     => "int(10) NOT NULL default '0'"
		),
		'errors' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_mod_queue_monitor']['errors'],
			'inputType'               => 'text',
			'filter'				  => true,
			'sorting'				 => true,
			'eval'                    => array('maxlength'=>255, 'tl_class' => 'w50', 'readonly' => true),
			'sql'                     => "int(10) NOT NULL default '0'"
		),
		
	)
);

class tl_mod_queue_monitor extends Backend
{	
	/**
	 * Return all Queue Gateways
	 *
	 * @return array
	 */
	public function getQueues()
	{

		$objQueues = Gateway::findBy(array('type=?'), array('queue'));
		
		$return = array();
		if(!is_null($objQueues)) {
			while($objQueues->next()) {
				$return[$objQueues->id] = $objQueues->title;
			}
		}

		return $return;
	}
}
