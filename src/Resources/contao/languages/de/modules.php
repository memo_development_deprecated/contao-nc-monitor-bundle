<?php
/**
 * @package   NCMonitorBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

/** 
 * Back end modules 
 */ 
$GLOBALS['TL_LANG']['MOD']['mod_queue_monitor_manager'] 	= array('Queue Monitor', 'Queue Monitor Manager verwalten');
$GLOBALS['TL_LANG']['MOD']['queue_monitor'] = array('Queue Monitor', 'Überwachung des Notification Centers');
$GLOBALS['TL_LANG']['MOD']['queues'] = array('Warteschlangen', 'Queue Monitor Manager');