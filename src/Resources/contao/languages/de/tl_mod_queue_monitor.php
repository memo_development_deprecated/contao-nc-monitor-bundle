<?php
/**
 * @package   NCMonitorBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

/**
 * Legends
 */
 
$GLOBALS['TL_LANG']['tl_mod_queue_monitor']['general_legend'] 		= 'Warteschlangen Details';


/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_mod_queue_monitor']['domain'] 				= array('Domäne','Welche Webseite ist betroffen?');
$GLOBALS['TL_LANG']['tl_mod_queue_monitor']['queue_id'] 				= array('Warteschlange','Welche Queue ist betroffen?');
$GLOBALS['TL_LANG']['tl_mod_queue_monitor']['last_cron'] 				= array('Letzter Status','Wann wurde das letzte Mal ein Status gewechselt?');
$GLOBALS['TL_LANG']['tl_mod_queue_monitor']['last_api_result'] 				= array('Letzte API Antwort','Was hat die API beim letzten Status-Wechsel geantwortet?');
$GLOBALS['TL_LANG']['tl_mod_queue_monitor']['errors']   				= array('Fehlerhafte Mails', "Anzahl Fehler in der Warteschlange");
$GLOBALS['TL_LANG']['tl_mod_queue_monitor']['queued']   				= array('Unversendete Mails', "Anzahl unversendete Mails in der Warteschlange");

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_mod_queue_monitor']['new']   					= array('Neue Warteschlange', 'Neue Warteschlange anlegen');
$GLOBALS['TL_LANG']['tl_mod_queue_monitor']['show']   				= array('Details', 'Infos zur Warteschlange mit der ID %s');
$GLOBALS['TL_LANG']['tl_mod_queue_monitor']['edit']   				= array('Warteschlange bearbeiten ', 'Warteschlange bearbeiten');
$GLOBALS['TL_LANG']['tl_mod_queue_monitor']['cut']    				= array('Warteschlange Verschieben', 'ID %s verschieben');
$GLOBALS['TL_LANG']['tl_mod_queue_monitor']['copy']  					= array('Warteschlange Duplizieren ', 'ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_mod_queue_monitor']['delete']					= array('Warteschlange Löschen ', 'ID %s löschen');